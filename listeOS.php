<!DOCTYPE html>
<html lang="fr">
    <?php
        session_start();
        include "fonctionDB.php";

        if ($_POST['logout']) {
            session_destroy();
            header("Location:index.php");
        }
    ?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="MSDN LLB Keys">
        <meta name="author" content="LLB">
        <link rel="shortcut icon" href="favicon.ico">
        <title>MSKeys LLB</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        
        <!--Bootstrap core JavaScript-->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
    <?php

        $connexion = connect();
        sessionConnexion($connexion);
	$tab_os = recupOS($connexion);

    ?>
    <div class="container">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">MSKeys LLB</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <?php
                    if (($_SESSION['login']) and ($_SESSION['password'])){
                ?>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Accueil</a></li>
                        <li><a href="importations.php">Importations</a></li>
			<li ><a href="gestOS.php">Gestion d'OS</a></li>
                <li><a href="attribuees.php">Clés utilisées</a></li>
			<li><a href="statistiques.php">Statistiques</a></li>
                        <li><a href="prefCompte.php">Paramètres</a></li>
                    </ul>
		    <form class="navbar-form navbar-right" role="form" action="index.php" method="post">
                        <input class="btn btn-warning" name="logout" type="submit" value="Déconnexion"></input>
                    </form><?php
                } else {?>
                <form class="navbar-form navbar-right" role="form" action="index.php" method="post">
                    <div class="form-group">
                    <input name="login" type="text" placeholder="Login" class="form-control">
                    </div>
                    <div class="form-group">
                    <input name="password" type="password" placeholder="Password" class="form-control">
                    </div>
                    <input name="submit_session" type="submit" class="btn btn-success" value="Connexion"></input>
                </form>
                <?php } ?>
            </div><!--/.navbar-collapse -->
        </nav>
    </div>
