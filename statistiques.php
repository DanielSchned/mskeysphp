<html>

  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <link href="css/stat.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
  </head>
  <body>
          <!-- MENU -->
    
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">MSKeys LLB</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Accueil</a></li>
                <li><a href="importations.php">Importations</a></li>
		<li ><a href="gestOS.php">Gestion d'OS</a></li>
                <li class="active"><a href="statistiques.php">Statistiques</a></li>
		<li><a href="prefCompte.php">Paramètres</a></li>
		<li ><a href="administration.php">Administration</a></li>
            </ul>
            <form class="navbar-form navbar-right" role="form" action="index.php" method="post">
                <input class="btn btn-warning" name="logout" type="submit" value="Déconnexion"></input>
            </form>
        </div><!-- /.navbar-collapse -->
    </nav>
    
      
  <?php 
  session_start();
  if(isset($_SESSION["login"])){
    include 'fonctionDB.php';
    $connexion = connect();
    afficheStatParOS($connexion);
    $res = afficheStatParUser($connexion);
    $res2 = afficheStatParSTA($connexion);

    echo '<div class="container statByUser"> <div class="well"><h3>Nombre de clefs attribuées en fonction des utilisateurs : </h3>';
    while ($data = $res->fetch()) {
      $percent = $data["NbKeys"];
      echo '
      <hr>
      Email : '.$data["email"].'
          <div class="row"><br />
              <div class="col-md-12">
                  <div class="prg">
                      <div class="prg success-color" style="width: '.$percent.'%;">
                          <div class="success-label">'.$data["NbKeys"].'
                          </div>
                      </div>
                  </div>
              </div>
          </div>';      
     }  
     echo '</div></div>';
  
    $res5 = afficheOS($connexion);

    echo '<div class="container statByUser"> <div class="well"><h3>Nombre de clefs enregistrées (attribuées ou non) en fonction des OS : </h3>';
    while ($data = $res5->fetch()) {
      $percent = $data["nb"];
      echo '
      <hr>
      OS : '.$data["name"].'
          <div class="row"><br />
              <div class="col-md-12">
                  <div class="prg">
                      <div class="prg success-color" style="width: '.$percent.'%;">
                          <div class="success-label">'.$data["nb"].'
                          </div>
                      </div>
                  </div>
              </div>
          </div>';
     }
     echo '</div></div>';
 
    $res3 = afficheStatUtiOS($connexion);

    echo '<div class="container statByUser"> <div class="well"><h3>Nombre de clefs attribuées en fonction des OS : </h3>';
    while ($data = $res3->fetch()) {
      $percent = $data["nbUti"];
      echo '
      <hr>
      OS : '.$data["name"].'
          <div class="row"><br />
              <div class="col-md-12">
                  <div class="prg">
                      <div class="prg success-color" style="width: '.$percent.'%;">
                          <div class="success-label">'.$data["nbUti"].'
                          </div>
                      </div>
                  </div>
              </div>
          </div>';
     }
     echo '</div></div>';
 
    $res4 = afficheStatDispoOS($connexion);

    echo '<div class="container statByUser"> <div class="well"><h3>Nombre de clefs disponibles en fonction des OS : </h3>';
    while ($data = $res4->fetch()) {
      $percent = $data["nbDispo"];
      echo '
      <hr>
      OS : '.$data["name"].'
          <div class="row"><br />
              <div class="col-md-12">
                  <div class="prg">
                      <div class="prg success-color" style="width: '.$percent.'%;">
                          <div class="success-label">'.$data["nbDispo"].'
                          </div>
                      </div>
                  </div>
              </div>
          </div>';
     }
     echo '</div></div>';
     
    echo '<div class="container statByUser"> <div class="well"><h3>STA pour lesquelles au moins 2 clés ont été attribuées : </h3>';
    while ($data = $res2->fetch()) {
      $percent = $data["NbKeys"];
      echo '
      <hr>
      OS : '.$data["nomSTA"].'
          <div class="row"><br />
              <div class="col-md-12">
                  <div class="prg">
                      <div class="prg success-color" style="width: '.$percent.'%;">
                          <div class="success-label">'.$data["NbKeys"].'
                          </div>
                      </div>
                  </div>
              </div>
          </div>';
     }
     echo '</div></div>';
     

     //bloc de code pour l'affichage du pourcentage de licences Windows 7 utilisées
    $res6 = pourcentSeven($connexion);

    echo '<div class="container statByUser"> <div class="well"><h3>Pourcentage de licences Windows 7 utilisées : </h3>';
    while ($data = $res6->fetch()) {
      $percent = $data["pourcent"];
      echo '
      <hr>
      OS : Microsoft Windows 7 Professionnel
          <div class="row"><br />
              <div class="col-md-12">
                  <div class="prg">
                      <div class="prg success-color" style="width: '.$percent.'%;">
                          <div class="success-label">'.$data["pourcent"].'
                          </div>
                      </div>
                  </div>
              </div>
          </div>';
     }
     echo '</div></div>';

	
	
	//bloc de code pour l'affichage des licences utilisées par salle
	
    $res7 = afficheSalle($connexion);

    echo '<div class="container statByUser"> <div class="well"><h3>Nombre de licences Windows 7 utilisées par salle : </h3>';
    while ($data = $res7->fetch()) {
      $percent = $data["nbSalle"];
      echo '
      <hr>
      OS : '.$data["salle"].'
          <div class="row"><br />
              <div class="col-md-12">
                  <div class="prg">
                      <div class="prg success-color" style="width: '.$percent.'%;">
                          <div class="success-label">'.$data["nbSalle"].'
                          </div>
                      </div>
                  </div>
              </div>
          </div>';
     }
     echo '</div></div>';  
 
  }
  else{
      header("Location:index.php");
  }
   ?>
  

  </body>

</html>

