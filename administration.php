<html>

  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <link href="css/stat.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
  </head>
  <body>
          <!-- MENU -->
    <?php
        session_start();
        include "fonctionDB.php";
        
        $connexion = connect();
        sessionConnexion($connexion);
        

    ?>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">MSKeys LLB</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Accueil</a></li>
                <li><a href="importations.php">Importations</a></li>
                <li ><a href="gestOS.php">Gestion d'OS</a></li>
                <li ><a href="statistiques.php">Statistiques</a></li>
                <li><a href="prefCompte.php">Paramètres</a></li>
		<li class="active"><a href="administration.php">Administration</a></li>
            </ul>
            <form class="navbar-form navbar-right" role="form" action="index.php" method="post">
                <input class="btn btn-warning" name="logout" type="submit" value="Déconnexion"></input>
            </form>
			
        		</div><!-- /.navbar-collapse -->
    		</nav>
	<center><div class="container" style = "padding-top: 22%" ><br><br><br>
         <form role="form" action="administration.php" method="post"> 
          <input class="btn btn-info" type="submit" name="btnpref" value="Nettoyer"/>
	</form>
        </center></div>
	<?php
		if(isset($_POST['btnpref'])){
          	 $result =  $connexion->exec("CALL historiquecle()");
          	 
	   	if($result == false){
                	echo "La bdd a été mise a jour";
		}
	}
	?>	
	</body>	
</html>    
